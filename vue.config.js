module.exports = {
  transpileDependencies: ["vuetify"],
  lintOnSave: false,
  lintOnCommit:false
};
